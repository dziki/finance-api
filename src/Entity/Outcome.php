<?php
/**
 * Created by PhpStorm.
 * User: aszymczyk
 * Date: 12/6/17
 * Time: 11:47 AM
 */

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Outcome
 * @package App\Entity
 * @ApiResource()
 * @ORM\Entity()
 */
class Outcome extends BudgetItem
{

}