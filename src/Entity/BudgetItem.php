<?php
/**
 * Created by PhpStorm.
 * User: aszymczyk
 * Date: 12/6/17
 * Time: 11:45 AM
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BudgetItem
 * @package App\Entity
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"income" = "Income", "outcome" = "Outcome"})
 * @ORM\Table(name="budget_item")
 */
abstract class BudgetItem
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var BudgetItemCategory
     * @ORM\ManyToOne(targetEntity="App\Entity\BudgetItemCategory")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BudgetItem
     */
    public function setName(string $name): BudgetItem
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return BudgetItemCategory
     */
    public function getCategory(): BudgetItemCategory
    {
        return $this->category;
    }

    /**
     * @param BudgetItemCategory $category
     * @return BudgetItem
     */
    public function setCategory(BudgetItemCategory $category): BudgetItem
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BudgetItem
     */
    public function setDescription(string $description): BudgetItem
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return BudgetItem
     */
    public function setCreatedAt(\DateTime $createdAt): BudgetItem
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return BudgetItem
     */
    public function setUpdatedAt(\DateTime $updatedAt): BudgetItem
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}