<?php
/**
 * Created by PhpStorm.
 * User: aszymczyk
 * Date: 12/6/17
 * Time: 12:30 PM
 */

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BudgetItemType
 * @package App\Entity
 * @ApiResource()
 * @ORM\Entity()
 * @ORM\Table(name="budget_item_type")
 */
class BudgetItemCategory
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BudgetItemCategory
     */
    public function setName(string $name): BudgetItemCategory
    {
        $this->name = $name;
        return $this;
    }
}